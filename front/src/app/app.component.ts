import { Component } from '@angular/core';
// import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  serverData: JSON;
  employeeData: JSON;

  constructor() {
  }

  // test1() {
  //   this.httpClient.get('http://127.0.0.1:5002/').subscribe(data => {
  //     this.serverData = data as JSON;
  //     console.log(this.serverData);
  //   })
  // }
  //
  // test2() {
  //   this.httpClient.get('http://127.0.0.1:5002/employees').subscribe(data => {
  //     this.employeeData = data as JSON;
  //     console.log(this.employeeData);
  //   })
  // }
}
