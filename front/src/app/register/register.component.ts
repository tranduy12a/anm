import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { HttpClientModule, HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
// @ts-ignore
import{ GlobalConstants } from './common/global-constants';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, retry } from 'rxjs/operators'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  // username: string;
  // password: string;
  serverData: JSON;
  role: String;
  link: String;
  ngOnInit(): void {
    this.role = "normal"
    this.loginForm.value.role = "normal"
  }
  loginForm = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    password: new FormControl(''),
    role: new FormControl('', [
      Validators.required,
    ]),
  });
  onSubmit() {
    console.warn(GlobalConstants.apiURL);
    // "role": this.loginForm.value.role
    let param = {"user_name": this.loginForm.value.username, "password": this.loginForm.value.password }

    let url = "http://0.0.0.0:5555/authentication/auth/register"


    let x = this.httpClient.get("http://0.0.0.0:5555/authentication/auth/register").subscribe(data => {
      console.log(data);
      // this.serverData = data as JSON;
      // // @ts-ignore
      // console.warn(this.serverData);
      // this.router.navigate(['/users'],)
    })
    console.log(x);
    // if(this.loginForm.value.username == "admin" && this.loginForm.value.password == "admin123") {
    //   alert("oke success!!!")
    //   this.router.navigate(['/login'],)
    // } else {
    //   alert("Lại đi!")
    // }
    // console.warn(this.loginForm);
  }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  onSubmit2(): Observable<any> {
    console.log(1111)
    return this.httpClient.get<any>("http://0.0.0.0:5555/authentication/auth/register", this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  };
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
  constructor(private fb: FormBuilder, private router: Router, private httpClient: HttpClient) { }
}

