import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {Router} from "@angular/router";

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private httpClient: HttpClient, private router: Router ) { }
  serverData: JSON;
  dataSource: JSON;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  ngOnInit(): void {
    this.getListUser()
  }
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  // dataSource = ELEMENT_DATA;
  getListUser() {
    this.httpClient.get("http://127.0.0.1:5002/users").subscribe(data => {
      this.serverData = data as JSON;
      this.dataSource =  data as JSON;

      console.log(this.serverData);
    })
  }
  goToaddAccount() {
    this.router.navigate(['/add-account'],)
  }
  goUpdateAddAccount(userName) {
    this.router.navigate(['/add-account/' + userName],)
  }
  deleteUser(userName) {
    let param = { "user_name": userName }
    // @ts-ignore
    this.httpClient.delete("http://127.0.0.1:5002/users", param).subscribe(data => {
      this.serverData = data as JSON;
      // @ts-ignore
      if (this.serverData == "success") {
        alert("Delete thanh cong user")
      } else {
        alert(this.serverData)
      }
      console.warn(this.serverData);
    })
  }
}
