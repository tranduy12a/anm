import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // username: string;
  // password: string;

  ngOnInit(): void {
  }
  loginForm = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    password: new FormControl(''),
  });
  onSubmit() {
    if(this.loginForm.value.username == "admin" && this.loginForm.value.password == "admin123") {
      alert("oke success!!!")
      this.router.navigate(['/users'],)
    } else {
      alert("Lại đi!")
    }
    console.warn(this.loginForm);
  }
  constructor(private fb: FormBuilder, private router: Router) { }
}
