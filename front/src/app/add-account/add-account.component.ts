import {Component, NgModule, OnInit} from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';

interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})

export class AddAccountComponent implements OnInit {

  constructor(private fb: FormBuilder, private router: Router, private httpClient: HttpClient, private activatedRoute: ActivatedRoute) {
  }
  // username: string;
  // password: string;
  serverData: JSON;
  employeeData: JSON;
  id: String;
  titlePage: String;
  btnName: String;
  user_name: String;
  password: String;
  role: String;

  ngOnInit(): void {
    // this.activatedRoute.params.subscribe(paramsId => {
    //   this.id = paramsId.id;
    // });
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (!this.id) {
      this.titlePage = "Add User"
      this.btnName = "Add Account"
    } else {
      this.titlePage = "Update User"
      this.btnName = "Update Account"
      this.getUserId()
    }
  }

  loginForm = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    password: new FormControl(''),
    role: new FormControl('user'),
  });

  onSubmit() {
    if (this.loginForm.value.username == "duyduy" && this.loginForm.value.password == "duyduy") {
      alert("dang nhap oke may")
      this.router.navigate(['/user'],)
    } else {
      alert("cut di may, sai r!")
    }
    console.warn(this.loginForm);
  }
  getUserId() {
    // let param = {"id": this.id }
    // @ts-ignore
    this.httpClient.get("http://127.0.0.1:5002/users/detail?id=" + this.id).subscribe(data => {
      // @ts-ignore
      this.serverData = data as JSON;
      // @ts-ignore
      if (this.serverData == "404") {
        alert("Account not exists!")
      } else {
        // this.loginForm.value.user_name = this.serverData[0].user_name
        this.user_name = this.serverData[0].user_name
        this.password = this.serverData[0].password
        this.role = this.serverData[0].role
      }
      // @ts-ignore
      console.warn(this.serverData);
      // this.router.navigate(['/users'],)
    })
  }
  addAccount() {
    let param = {"user_name": this.loginForm.value.username, "password": this.loginForm.value.password, "role": this.loginForm.value.role }
    if (!this.id) {
      this.httpClient.post("http://127.0.0.1:5002/users", param).subscribe(data => {
        this.serverData = data as JSON;
        // @ts-ignore
        if (this.serverData == "success") {
          alert("Them thanh cong user")
        } else {
          alert(this.serverData)
        }
        // this.router.navigate(['/users'],)
      })
    } else {
      let param = {"user_name": this.loginForm.value.username, "password": this.loginForm.value.password, "role": this.loginForm.value.role }
      this.httpClient.put("http://127.0.0.1:5002/users", param).subscribe(data => {
        this.serverData = data as JSON;
        // @ts-ignore
        if (this.serverData == "success") {
          alert("Update thanh cong user")
        } else {
          alert(this.serverData)
        }
        console.warn(this.serverData);
      })
    }
  }
  backPage() {
    this.router.navigate(['/users'],)
  }
}
