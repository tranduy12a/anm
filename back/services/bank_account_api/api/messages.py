#!/usr/bin/env python3
# -*- coding: utf-8 -*-

ERROR_MESSAGES = {
    'E0000': 'Bad request.',
    'E0101': 'Insecure request is disallowed.',
    'E0102': 'Request body must be valid JSON.',
    'E0103': 'Request must use POST method.',
    'E0104': 'Invalid request url.',
    'E0105': 'There\'s a problem from server.',
    'E2001': 'All operations are already permitted.',
    'E3047': 'Invalid `last_name` parameter.'
}
