#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
import decimal
import logging
from logging import handlers
import math
import os
import sys
import subprocess
import re

from api import (
    BankApiError,
    app
)
from api.models import (
    User,
    BankAccount
)
from flask import (
    g,
    request
)
import jwt
import requests


logger = logging.getLogger(__name__)

def encode_auth_token(user_id):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            app.config.get('SECRET_KEY'),
            algorithm='HS256'
        )
    except Exception as e:
        return e


def decode_auth_token(auth_token):
    """
    Validates the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'))
        return payload
    except jwt.ExpiredSignatureError:
        return 'Your token is expired. Please log in again.'
    except jwt.InvalidTokenError:
        return 'Invalid token. Please log in again.'


def api_authorization_check():
    """
    Check valid request

    Returns:
        flask.Response
    """
    request_data = None
    user = None

    if ((request.headers.get('X-Forwarded-Proto') is not None and
            request.headers.get('X-Forwarded-Proto') == 'http')):
        raise BankApiError(error_code='E0101', status_code=400)

    if not request.data:
        raise BankApiError(error_code='E0102', status_code=400)

    request_data = request.get_json(silent=True)
    if request_data is None:
        raise BankApiError(error_code='E0102', status_code=400)

    # TODO: Decode JWT Token <user_id>
    # Query by user_id
    # user = User.query.filter_by(user_id=user_id).first()
    # if not user:
    #     raise BankApiError(error_code='E3039')

    logger.info(request_data)

    g.request_data = request_data
    g.current_user = user
