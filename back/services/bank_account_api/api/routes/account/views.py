#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

from api import bcrypt
from api.models import User
from api.utils.common import (
    decode_auth_token,
    encode_auth_token
)
from flask import (
    Blueprint,
    jsonify,
    make_response,
    request
)
from flask.views import MethodView


logger = logging.getLogger(__name__)
auth_blueprint = Blueprint('auth', __name__)


class RegisterAPI(MethodView):
    """
    User Registration Resource
    """

    def post(self):
        post_data = request.get_json()
        email = post_data.get('email')
        password = post_data.get('password')

        user = User.query.filter_by(email=email).first()

        if not user:
            try:
                current_user_sequence = UserSequence.query.first()
                next_user_sequence = current_user_sequence.id + 1
                user = User(
                    email=email,
                    password=password,
                    user_sequence=next_user_sequence
                )

                db.session.add(user)
                db.session.add(wallet)
                db.session.add(user_balance)
                db.session.commit()
                auth_token = encode_auth_token(str(user.id))
                response_object = {
                    'success': 1,
                    'message': 'Registration successfully.',
                    'auth_token': auth_token.decode()
                }
                return make_response(jsonify(response_object)), 201
            except Exception as error:
                response_object = {
                    'success': 0,
                    'message': 'Registration failed.'
                }
                logger.exception(error)
                return make_response(jsonify(response_object)), 401
        else:
            response_object = {
                'success': 0,
                'message': 'Email has been existed.',
            }
            return make_response(jsonify(response_object)), 202


class LoginAPI(MethodView):
    """
    User Login Resource
    """
    def get(self):
        response_object = {
            'success': 0,
            'message': 'djt me may'
        }
        return make_response(jsonify(response_object)), 500
    def post(self):
        post_data = request.get_json()
        email = post_data.get('email')
        password = post_data.get('password')

        try:
            user = User.query.filter_by(email=email).first()

            if not user:
                response_object = {
                    'success': 0,
                    'message': "User isn't existed."
                }
                return make_response(jsonify(response_object)), 404

            if not user.status:
                response_object = {
                    'success': 0,
                    'message': "Your account has been locked."
                }
                return make_response(jsonify(response_object)), 401

            if bcrypt.check_password_hash(user.password, password):
                auth_token = encode_auth_token(str(user.id))
                if auth_token:
                    response_object = {
                        'success': 1,
                        'message': 'Login successfully.',
                        'auth_token': auth_token.decode()
                    }
                    return make_response(jsonify(response_object)), 200
            else:
                response_object = {
                    'success': 0,
                    'message': 'Incorrect password.'
                }
                return make_response(jsonify(response_object)), 404

        except Exception as error:
            logger.exception(error)

        response_object = {
            'success': 0,
            'message': 'Login failed. Please try again.'
        }
        return make_response(jsonify(response_object)), 500



# Define the API resources
registration_view = RegisterAPI.as_view('register_api')
login_view = LoginAPI.as_view('login_api')

# Add Rules for API Endpoints
auth_blueprint.add_url_rule(
    '/auth/register',
    view_func=registration_view,
    methods=['POST']
)
auth_blueprint.add_url_rule(
    '/auth/login',
    view_func=login_view,
    methods=['POST']
)
auth_blueprint.add_url_rule(
    '/auth/login',
    view_func=login_view,
    methods=['GET']
)
