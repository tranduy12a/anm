#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import uuid

from api import (
    app,
    bcrypt
)

BASE_TIME_FORMAT = '%Y/%m/%d %H:%M:%S'
DEFAULT_LIMIT = 10
DEFAULT_SORT_ORDER = 'asc'
USER_NORMAL_ROLE = 1
USER_ADMIN_ROLE = 10
BANK_ACCOUNT_SEARCHABLE_FIELDS = [
    'account_number',
    'balance'
]


class User:
    """
    User Model

    Variables:
        __tablename__ {str} -- Table name

        id {str} -- User Id (UUID)
        username {str} -- Username
        password {str} -- Hashed Password
        role {int} -- User Role: 0 - Normal, 1 - Manager
    """
    __tablename__ = 'user-login'

    pass

    def __init__(self, user_name, password, role):
        self.id = uuid.uuid4()
        self.user_name = user_name
        self.email = email
        self.password = bcrypt.generate_password_hash(
            password, app.config.get('BCRYPT_LOG_ROUNDS')
        ).decode()
        #self.role = role
        #self.created_at = datetime.datetime.utcnow()
        #self.updated_at = datetime.datetime.utcnow()

class BankAccount:
    """
    BankAccount Model

    Variables:
        __tablename__ {str} -- Table name

        account_number {int} -- Account Number
        balance {int} -- Account Balance
        firstname {str} -- First Name
        lastname {str} -- Last Name
        age {int} -- Age
        gender {str} -- Gender
        address {str} -- Address
        employer {str} -- Employer
        email {str} -- Email
        city {str} -- City
        state {str} -- State
        created_at {datetime.datetime} -- Creation time
        updated_at {datetime.datetime} -- Last updated time
    """
    __tablename__ = 'bank-account'

    pass

    def __init__(self):
        pass
