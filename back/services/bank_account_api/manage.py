#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import logging
import os
import subprocess
import sys
import uuid
import math
import random

from api import app
from flask_script import (
    Manager,
    Server
)

import requests

logger = logging.getLogger(__name__)
http_address = app.config.get('HTTP_ADDRESS')
http_port = app.config.get('HTTP_PORT')

manager = Manager(app)
manager.add_command('runserver', Server(host=http_address, port=http_port))


@manager.command
def create_db():
    """
    Create collections

    Decorators:
        manager.command
    """
    logger.info('create_db command was executed')
    if app.config.get('PRODUCTION'):
        print('DB couldn\'t create on production mode.')
        return

    pass
    print('[create_db] Done')


@manager.command
def seed_db():
    """
    Seed DB with accounts.json

    Decorators:
        manager.command
    """
    pass


@manager.command
def drop_db():
    """
    Drop collections

    Decorators:
        manager.command
    """
    logger.info('drop_db command was executed')
    if app.config.get('PRODUCTION'):
        print('DB couldn\'t drop on production mode.')
        return

    print('[drop_db] Done')


if __name__ == '__main__':
    manager.run()
